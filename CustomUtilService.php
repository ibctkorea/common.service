<?php


namespace App\Service\common;

use App\Repositories\CurrencyInfoRepository;




class CustomUtilService
{

    /**
     * @var CurrencyInfoRepository
     */
    private $currencyInfoRepository;

    public function __construct(CurrencyInfoRepository $currencyInfoRepository)
    {
        $this->currencyInfoRepository = $currencyInfoRepository;

    }

    public function unsetPage($arrPage)
    {
        unset($arrPage['first_page_url']);
        unset($arrPage['from']);
        unset($arrPage['last_page_url']);
        unset($arrPage['next_page_url']);
        unset($arrPage['path']);
        unset($arrPage['prev_page_url']);
        unset($arrPage['to']);
        return $arrPage;
    }

    public function getCoinList()
    {
        $coinList = $this->currencyInfoRepository->getCoinInfo();
        $temp = [];

        $temp[0]['label'] = '전체';
        $temp[0]['id'] = '';

        foreach ($coinList as $index => $coin) {
            $temp[$index+1]['label'] = $coin->symbol;
            $temp[$index+1]['id'] = $coin->id;
        }

        return $temp;
    }
}
