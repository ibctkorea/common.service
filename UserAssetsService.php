<?php


namespace App\Service\common;


use App\Exceptions\ApplicationException;
use App\Models\UsedAssets;
use App\Models\UserAssetsLog;
use App\Repositories\CurrencyInfoRepository;
use App\Repositories\CurrencyUserRepository;
use App\Repositories\TradeContractRepository;
use App\Repositories\UsedAssetsRepository;
use App\Repositories\UserAssetsLogRepository;
use Exception;
use Illuminate\Support\Facades\DB;

class UserAssetsService
{
    /**
     * @var CurrencyUserRepository
     */
    private $currencyUserRepository;
    /**
     * @var UserAssetsLogRepository
     */
    private $userAssetsLogRepository;
    /**
     * @var UsedAssetsRepository
     */
    private $usedAssetsRepository;
    /**
     * @var CurrencyInfoRepository
     */
    private $currencyInfoRepository;
    /**
     * @var TradeContractRepository
     */
    private $tradeContractRepository;
    /**
     * @var CurrencyInfoService
     */
    private $currencyInfoService;

    public function __construct(CurrencyUserRepository $currencyUserRepository, UserAssetsLogRepository $userAssetsLogRepository, UsedAssetsRepository $usedAssetsRepository, CurrencyInfoRepository $currencyInfoRepository, TradeContractRepository $tradeContractRepository, CurrencyInfoService $currencyInfoService) {
        $this->currencyUserRepository = $currencyUserRepository;
        $this->userAssetsLogRepository = $userAssetsLogRepository;
        $this->usedAssetsRepository = $usedAssetsRepository;
        $this->currencyInfoRepository = $currencyInfoRepository;
        $this->tradeContractRepository = $tradeContractRepository;
        $this->currencyInfoService = $currencyInfoService;
    }

    /**
     * 회원 자산 조회 (Lock 처리 추가)
     *
     * @param $currencyId
     * @param $memberId
     * @return mixed
     */
    public function getCurrencyUserLock($currencyId, $memberId)
    {
        return $this->currencyUserRepository->where([
            'currency_id' => $currencyId,
            'member_id' => $memberId,
        ])->lockForUpdate()->first();
    }

    /**
     * 회원 자산 조회
     * @param $currencyId
     * @param $memberId
     * @return mixed
     */
    public function getCurrencyUser($currencyId, $memberId)
    {
        return $this->currencyUserRepository->where([
            'currency_id' => $currencyId,
            'member_id' => $memberId,
        ])->first();
    }

    /**
     * 자산 증가 처리 (자산로그 등록 포함)
     *
     * @param $changeType
     * @param $currencyId
     * @param $memberId
     * @param $amount
     * @param $refId
     */
    public function addCurrencyUserAsset($changeType, $currencyId, $memberId, $amount, $refId)
    {
        list($beforeAmount,) = $this->addAssetAmount($currencyId, $memberId, $amount);
        $this->createAssetLog($changeType, $currencyId, $memberId, $amount, $refId, $beforeAmount);
    }

    /**
     * 자산 증가 트랜잭션 처리 (증거금 등록 포함)
     *
     * @param $changeType
     * @param $currencyId
     * @param $memberId
     * @param $amount
     * @param $refId
     */
    public function addUserAssetTransaction($changeType, $currencyId, $memberId, $amount, $refId)
    {
        try {
            DB::beginTransaction();
            // 회원자산 처리
            list($beforeAmount,) = $this->addAssetAmount($currencyId, $memberId, $amount);
            // 회원 자산 로그
            $this->createAssetLog($changeType, $currencyId, $memberId, $amount, $refId, $beforeAmount);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * 자산 증가
     *
     * @param $currencyId
     * @param $memberId
     * @param $amount
     * @return mixed
     */
    public function addAssetAmount($currencyId, $memberId, $amount)
    {
        // lock
        $targetCurrencyUser = $this->getCurrencyUserLock($currencyId, $memberId);
        $beforeAmount = $targetCurrencyUser->num;
        $targetCurrencyUser->num = bcadd($targetCurrencyUser->num, $amount, 18);
        $targetCurrencyUser->save();
        return [$beforeAmount, $targetCurrencyUser];
    }

    /**
     * 자산 차감 처리 (자산로그 등록 포함)
     *
     * @param $changeType
     * @param $currencyId
     * @param $memberId
     * @param $amount
     * @param $refId
     */
    public function subCurrencyUserAsset($changeType, $currencyId, $memberId, $amount, $refId)
    {
        list($beforeAmount,) = $this->subAssetAmount($currencyId, $memberId, $amount);
        $this->createAssetLog($changeType, $currencyId, $memberId, $amount, $refId, $beforeAmount);
    }

    /**
     * 자산 차감 트랜잭션 처리 (자산로그 등록 포함)
     *
     * @param $changeType
     * @param $currencyId
     * @param $memberId
     * @param $amount
     * @param $refId
     */
    public function subUserAssetTransaction($changeType, $currencyId, $memberId, $amount, $refId)
    {
        try {
            DB::beginTransaction();
            // 회원자산 처리
            list($beforeAmount,) = $this->subAssetAmount($currencyId, $memberId, $amount);
            // 회원 자산 로그
            $this->createAssetLog($changeType, $currencyId, $memberId, $amount, $refId, $beforeAmount);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * 자산 차감
     *
     * @param $currencyId
     * @param $memberId
     * @param $amount
     * @return mixed
     */
    public function subAssetAmount($currencyId, $memberId, $amount)
    {
        // lock
        $targetCurrencyUser = $this->getCurrencyUserLock($currencyId, $memberId);
        $beforeAmount = $targetCurrencyUser->num;
        $targetCurrencyUser->num = bcsub($targetCurrencyUser->num, $amount, 18);
        $targetCurrencyUser->save();
        return [$beforeAmount, $targetCurrencyUser];
    }

    /**
     * 자산 로그 등록
     *
     * @param $changeType
     * @param $currencyId
     * @param $memberId
     * @param $amount
     * @param $refId
     * @param $beforeAmount
     * @return UserAssetsLog
     */
    public function createAssetLog($changeType, $currencyId, $memberId, $amount, $refId, $beforeAmount): UserAssetsLog
    {
        // 회원 자산 로그
        $assetsParams = [
            'type' => $changeType,
            'member_id' => $memberId,
            'currency_id' => $currencyId,
            'before_amount' => $beforeAmount,
            'amount' => $amount,
            'ref_id' => $refId,
        ];
        return $this->userAssetsLogRepository->create($assetsParams);
    }

    /**
     * 회원 코인 자산 조회
     *
     * @param $currencyId
     * @param $memberId
     * @return float
     */
    public function getUsedAssets($currencyId, $memberId)
    {
        return $this->usedAssetsRepository->where([
            'currency_id' => $currencyId,
            'member_id' => $memberId
        ])->sum('amount');
    }

    /**
     * 회원 코인 자산 조회 (LOCK)
     *
     * @param $currencyId
     * @param $memberId
     * @return float
     */
    public function getUsedAssetsLock($currencyId, $memberId)
    {
        return $this->usedAssetsRepository->where([
            'currency_id' => $currencyId,
            'member_id' => $memberId
        ])->lockForUpdate()->sum('amount');
    }

    /**
     * 사용가능 자산 조회
     *
     * @param $memberId
     * @param $currencyId
     * @param $decimalPoint
     * @return String
     */
    public function getAvailableUserAsset($memberId, $currencyId, $decimalPoint=null)
    {
        $usedAsset = $this->getUsedAssets($currencyId, $memberId);
        $currencyUser = $this->getCurrencyUser($currencyId, $memberId);
        $amount = empty($currencyUser) ? '0' : $currencyUser->num;
        if (is_null($decimalPoint)) {
            $currencyInfo = $this->currencyInfoRepository->find($currencyId);
            $decimalPoint = $currencyInfo->decimal_point;
        }

        return bcsub($amount, $usedAsset, $decimalPoint);
    }

    /**
     * 사용가능 자산 조회 (LOCK)
     *
     * @param $memberId
     * @param $currencyId
     * @param $decimalPoint
     * @return String
     */
    public function getAvailableUserAssetLock($memberId, $currencyId, $decimalPoint)
    {
        $usedAsset = $this->getUsedAssetsLock($currencyId, $memberId);
        $currencyUser = $this->getCurrencyUserLock($currencyId, $memberId);
        $amount = empty($currencyUser) ? '0' : $currencyUser->num;

        return bcsub($amount, $usedAsset, $decimalPoint);
    }

    /**
     * 증거금 설정 (자산 Lock)
     *
     * @param $memberId
     * @param $currencyId
     * @param $decimalPoint
     * @param $requestAmount
     * @param $type
     * @param $refId
     * @return int
     * @throws Exception
     */
    public function setUsedAssets($memberId, $currencyId, $decimalPoint, $requestAmount, $type, $refId)
    {
        try {
            DB::beginTransaction();

            $availableBalance = $this->getAvailableUserAssetLock($memberId, $currencyId, $decimalPoint);

            if (bccomp($availableBalance, $requestAmount, $decimalPoint) < 0) {
                throw new ApplicationException(ApplicationException::LOW_BALANCE);
            }

            $attributes = [
                'type' => $type,
                'member_id' => $memberId,
                'currency_id' => $currencyId,
                'amount' => $requestAmount,
                'ref_id' => $refId,
            ];

            $usedAssets = $this->usedAssetsRepository->create($attributes);
            $id = $usedAssets->id;

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return $id;
    }

    /**
     * 증거금 참조키 설정
     *
     * @param $id
     * @param $refId
     * @return int
     */
    public function setUsedAssetsRefId($id, $refId)
    {
        return $this->usedAssetsRepository->update($id, ['ref_id' => $refId]);
    }

    /**
     * 참조키로 증거금 조회
     * @param $type
     * @param $refId
     * @return UsedAssets
     */
    public function getUsedAssetsByRefId($type, $refId)
    {
        return $this->usedAssetsRepository->where([
            'type' => $type,
            'ref_id' => $refId,
        ])->first();
    }

    /**
     * 증거금 데이터 등록
     *
     * @param $params
     * @return int
     */
    public function createUsedAssets($params)
    {
        return $this->usedAssetsRepository->create($params);
    }

    /**
     * 증거금 데이터 삭제
     *
     * @param $type
     * @param $refId
     * @return int
     */
    public function deleteUsedAssets($type, $refId)
    {
        return $this->usedAssetsRepository->where([
            'type' => $type,
            'ref_id' => $refId,
        ])->delete();
    }

    /**
     * 회원자산 내역 조회
     *
     * @param $member_id
     * @return array
     */
    public function getMemberAssetList($member_id)
    {
        $memberAsset = $this->currencyUserRepository->where(['member_id' => $member_id])->get();
        $usedAsset = $this->usedAssetsRepository->getUsedAssetByMember($member_id);
        $currencyInfo = $this->currencyInfoRepository->where(['active' => 1])->get();
        $listedCurrency = $this->currencyInfoRepository->listedCurrency();

        $krwPrice[189] = 1;
        $krwPrice[185] = $this->currencyInfoService->getMarketPrice(189, 185);
        $krwPrice[187] = $this->currencyInfoService->getMarketPrice(189, 187);
        $krwPrice[202] = $this->currencyInfoService->getMarketPrice(202, 185);
        $krwPrice[202] = bcdiv($krwPrice[185], $krwPrice[202], 2);

        $lastPrice = [];
        foreach ($listedCurrency as $currency) {
            $price = $this->currencyInfoService->getMarketPrice($currency->market_info_id, $currency->currency_info_id);;
            $lastPrice[$currency->currency_info_id] = bcmul($krwPrice[$currency->market_info_id], $price, 18);
        }
        $lastPrice[189] = 1;
        $lastPrice[202] = $krwPrice[202];

        $assetResult = [];
        foreach ($memberAsset as $asset) {
            $assetResult[$asset->currency_id] = $asset->num;
        }

        $usedAssetResult = [];
        foreach ($usedAsset as $asset) {
            $usedAssetResult[$asset->currency_id] = $asset->amount;
        }

        $result = [];
        foreach ($currencyInfo as $key => $currency) {
            $amount = empty($assetResult[$currency->id]) ? '0' : $assetResult[$currency->id];
            $usedAsset = empty($usedAssetResult[$currency->id]) ? '0' : $usedAssetResult[$currency->id];
            $availableAmount = bcsub($amount, $usedAsset, $currency->decimal_point);
            $price = empty($lastPrice[$currency->id]) ? '0' : $lastPrice[$currency->id];

            $result[$currency->id] = [
                'symbol' => $currency->symbol,
                'currency_nm' => $currency->currency_nm_kr,
                'currency_nm_en' => $currency->currency_nm_en,
                'currency_nm_kr' => $currency->currency_nm_kr,
                'amount' => bcadd($amount, '0', $currency->decimal_point),
                'available_amount' => $availableAmount,
                'used_asset' => bcadd($usedAsset, '0', $currency->decimal_point),
                'currency_id' => $currency->id,
                'decimal_point' => $currency->decimal_point,
                'deposit_status' => $currency->deposit_status,
                'withdraw_status' => $currency->withdraw_status,
                'last_price' => $price,
                'amount_to_krw' => bcmul($amount, $price, 2)
            ];
        }

        return $result;
    }
}
