<?php


namespace App\Service\common;

use App\Repositories\BankInfoRepository;

class BankInfoService
{


    /**
     * @var BankInfoRepository
     */
    private $bankInfoRepository;

    public function __construct(BankInfoRepository $bankInfoRepository)
    {
        $this->bankInfoRepository = $bankInfoRepository;
    }
    public function getBankInfoList(){
        $bankList = $this->bankInfoRepository->all('bank_code','bank_name');
        $temp = [];
        foreach ($bankList as $index => $bank) {
            $temp[$index]['label'] = $bank->bank_name;
            $temp[$index]['id'] = $bank->bank_code;
        }
        return $temp;
    }
}
