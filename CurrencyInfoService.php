<?php


namespace App\Service\common;


use App\Repositories\CurrencyInfoRepository;
use App\Repositories\TradeContractRepository;
use Illuminate\Support\Facades\DB;

class CurrencyInfoService
{
    /**
     * @var CurrencyInfoRepository
     */
    private $currencyInfoRepository;
    /**
     * @var TradeContractRepository
     */
    private $tradeContractRepository;
    /**
     * @var array
     */
    private $marketPrice;

    public function __construct(CurrencyInfoRepository $currencyInfoRepository, TradeContractRepository $tradeContractRepository) {
        $this->currencyInfoRepository = $currencyInfoRepository;
        $this->tradeContractRepository = $tradeContractRepository;
    }

    /**
     * 현재 시세 조회
     *
     * @param $baseCurrencyId
     * @param $targetCurrencyId
     * @return double
     */
    public function getMarketPrice($baseCurrencyId, $targetCurrencyId)
    {
        if (!empty($this->marketPrice[$baseCurrencyId][$targetCurrencyId])) {
            return $this->marketPrice[$baseCurrencyId][$targetCurrencyId];
        }

        $condition = [
            'base_currency_id' => $baseCurrencyId,
            'target_currency_id' => $targetCurrencyId
        ];
        $orderBy = [
            'column' => 'id',
            'sort' => 'desc'
        ];
        $lastTradeContract = $this->tradeContractRepository->whereOrderByOne($condition, $orderBy);
        if (!empty($lastTradeContract->price)) {
            $this->marketPrice[$baseCurrencyId][$targetCurrencyId] = $lastTradeContract->price;
            return $this->marketPrice[$baseCurrencyId][$targetCurrencyId];
        }

        $targetCurrency = $this->currencyInfoRepository->marketCurrencyInfo($baseCurrencyId, $targetCurrencyId);
        $this->marketPrice[$baseCurrencyId][$targetCurrencyId] = $targetCurrency->listed_price;

        return $this->marketPrice[$baseCurrencyId][$targetCurrencyId];
    }


    public function getListedCurrencyInfo($attr){
        return $this->currencyInfoRepository->getListedCurrencyInfo($attr);
    }

}
